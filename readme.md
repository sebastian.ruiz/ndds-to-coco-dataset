# Coverting NDDS to COCO Dataset

Use the `ndds_to_coco.py` script. The classes are based on the mask colours used in the `.label.png` files.

First test wether it's producing what you want by setting `TESTING_STAGE=True`.

To check whether it worked properly, use: https://github.com/trsvchn/coco-viewer
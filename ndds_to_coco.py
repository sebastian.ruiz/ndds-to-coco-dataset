import datetime
import json
import os
import numpy as np
from pycococreatortools import pycococreatortools
import regex
from helpers import *
from tqdm import tqdm


DATA_DIR = '/media/sebastian/hdd11/datasets/ndds/07-12-2020-front-back-sides-battery'
JSON_PATH = os.path.join(DATA_DIR, "_coco.json")
TESTING_STAGE = False

INFO = {
    "description": "NDDS Dataset",
    "url": "https://github.com/sebastian-ruiz",
    "version": "0.1.0",
    "year": 2020,
    "contributor": "Sebastian Ruiz",
    "date_created": datetime.datetime.utcnow().isoformat(' ')
}

LICENSES = [
    {
        "id": 1,
        "name": "Attribution-NonCommercial-ShareAlike License",
        "url": "http://creativecommons.org/licenses/by-nc-sa/2.0/"
    }
]

CATEGORIES = [
    {
        'id': 0,
        'name': 'battery',
        'supercategory': 'kalo_1.5',
    },
    {
        'id': 2,
        'name': 'kalo_1.5_back',
        'supercategory': 'kalo_1.5',
    },
    {
        'id': 3,
        'name': 'kalo_1.5_front',
        'supercategory': 'kalo_1.5',
    },
    {
        'id': 4,
        'name': 'kalo_1.5_side1',
        'supercategory': 'kalo_1.5',
    },
    {
        'id': 5,
        'name': 'kalo_1.5_side2',
        'supercategory': 'kalo_1.5',
    },
    {
        'id': 6,
        'name': 'background',
        'supercategory': 'background',
    },
]

# list of labels that actually are the same class.
# e.g.: classes 0 and 1 in masks_label are yellow and green battery respectively
# but I want them to be of the same class.
# the first label in the list is the one that all the listed classes will have.
OVERRIDE_CLASS = [[0, 1]]


def listdir_nohidden(path):
    for f in os.listdir(path):
        if not f.startswith('.'):
            yield f

def main():
    coco_output = {
        "info": INFO,
        "licenses": LICENSES,
        "categories": CATEGORIES,
        "images": [],
        "annotations": []
    }

    total_is_id = 0

    data_dir = os.path.join(DATA_DIR)

    images_fps = [img for img in listdir_nohidden(data_dir) if img.endswith(".color.png")]
    images_fps.sort(key=lambda f: int(regex.sub('\D', '', f)))
    images_fps = np.array([os.path.join(data_dir, image) for image in images_fps])

    masks_label_fps = [img for img in listdir_nohidden(data_dir) if img.endswith(".label.png")]  # class segmentation
    masks_label_fps.sort(key=lambda f: int(regex.sub('\D', '', f)))
    masks_label_fps = np.array([os.path.join(data_dir, image) for image in masks_label_fps])

    masks_is_fps = [img for img in listdir_nohidden(data_dir) if img.endswith(".is.png")]  # instance segmentation
    masks_is_fps.sort(key=lambda f: int(regex.sub('\D', '', f)))
    masks_is_fps = np.array([os.path.join(data_dir, image) for image in masks_is_fps])

    images_to_iterate_over = np.arange(len(images_fps))
    if TESTING_STAGE:
        images_to_iterate_over = np.array([0])  # only the first one for testing
    for image_id in tqdm(images_to_iterate_over):
        if TESTING_STAGE:
            print("adding image " + str(image_id) + " to json.")
        image = cv2.imread(images_fps[image_id])
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        mask_label = cv2.imread(masks_label_fps[image_id], 0)
        mask_is = cv2.imread(masks_is_fps[image_id], 0)
        if TESTING_STAGE:
            print("images_fps[i]", images_fps[image_id])
            print("masks_label_fps[i]", masks_label_fps[image_id])
            print("masks_is_fps[i]", masks_is_fps[image_id])

        image_info = pycococreatortools.create_image_info(
            image_id, os.path.basename(images_fps[image_id]), tuple(image.shape[:2]))
        coco_output["images"].append(image_info)

        mask_label_colours = np.unique(mask_label)
        mask_label = [(mask_label == v) for v in mask_label_colours]
        mask_label = np.stack(mask_label, axis=-1).astype('float')
        if TESTING_STAGE:
            print("mask_label", mask_label.shape)

        mask_is_colours = np.unique(mask_is)
        mask_is = [(mask_is == v) for v in mask_is_colours]
        mask_is = np.stack(mask_is, axis=-1).astype('float')
        if TESTING_STAGE:
            print("mask_is_colours", mask_is_colours)
            print("mask_is", mask_is.shape)

        if TESTING_STAGE:
            visualize_masks(image, mask_label, title="label masks")
            print("mask_is.shape", mask_is.shape)
            visualize_masks(image, mask_is, title="instance segmentation masks")


        # we need to match the mask_label to the mask_is
        # is_labels gives the class of each instance segmented mask
        is_labels = np.full(mask_is.shape[-1], -1)  # array of -1s
        for is_id in np.arange(mask_is.shape[-1]):
            a_mask_is = mask_is[..., is_id]
            for k in np.arange(mask_label.shape[-1]):
                a_mask_label = mask_label[..., k]
                bitwise_and = cv2.bitwise_and(a_mask_is, a_mask_label)
                diff = np.abs(bitwise_and - a_mask_is)
                count_non_zero = np.count_nonzero(diff)
                if count_non_zero < 100:  # hardcoded margin of error!
                    # print("found mask class", k)

                    # check if we want this class to be a different one depending on SAME_CLASS
                    should_override_class = False
                    for classes_to_concatenate in OVERRIDE_CLASS:
                        if k in classes_to_concatenate:
                            should_override_class = True
                            is_labels[is_id] = classes_to_concatenate[0]  # apply first class label
                            break
                    if not should_override_class:
                        is_labels[is_id] = k

                    break

        for is_id in np.arange(mask_is.shape[-1]):
            category_info = {'id': int(is_labels[is_id]), 'is_crowd': False}

            annotation_info = pycococreatortools.create_annotation_info(
                total_is_id, image_id, category_info, mask_is[..., is_id].squeeze(), tolerance=2)

            if annotation_info is not None:
                coco_output["annotations"].append(annotation_info)

            total_is_id += 1

    with open(JSON_PATH, 'w') as output_json_file:
        indent = None
        if TESTING_STAGE:
            indent = 4

        json.dump(coco_output, output_json_file, indent=indent, sort_keys=True,
                  separators=(', ', ': '), ensure_ascii=False,
                  cls=NumpyEncoder)


if __name__ == "__main__":
    main()
